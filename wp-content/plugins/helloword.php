<?php
/*
Plugin Name: Hello World
Description: This is just a useless plugin.
Author: Greg
Version: 1.0.0
*/

// Write a new permalink entry on code activation
register_activation_hook( __FILE__, 'customop_activation' );
function customop_activation() {
        customop_custom_output();
        flush_rewrite_rules(); // Update the permalink entries in the database, so the permalink structure needn't be redone every page load
}

// If the plugin is deactivated, clean the permalink structure
register_deactivation_hook( __FILE__, 'customop_deactivation' );
function customop_deactivation() {
        flush_rewrite_rules();
}


// And now, the code that do the magic!!!
// This code create a new permalink entry
add_action( 'init', 'customop_custom_output' );
function customop_custom_output() {
        add_rewrite_tag( '%helloworld%', '([^/]+)' );
        add_permastruct( 'helloworld', '/%helloworld%' );
}

// The following controls the output content
add_action( 'template_redirect', 'customop_display' );
function customop_display() {
        if ($query_var = get_query_var('helloworld')) {
                header("Content-Type: text/plain");
                echo 'Hello World!';
                exit; // Don't forget the exit. If so, WordPress will continue executing the template rendering and will not fing anything, throwing the 'not found page' 
        }
}